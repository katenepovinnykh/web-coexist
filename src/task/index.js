import React from 'react';
import { Link } from 'react-router-dom';
import useStyles from './styles';
import Button from '@material-ui/core/Button';
import Visibility from '@material-ui/icons/Visibility';
import CloudDownloadIcon from '@material-ui/icons/CloudDownload';
import {getTasksData} from '../server';
import {NorppaIcon} from './norppaIcon';
import { useState, useEffect } from 'react';


export default function Task({match}) {
  const classes = useStyles();
  const taskId = match.params.taskId;
  const [data, setData] = useState([]);

  useEffect(() => {
    getTasksData().then(taskData => setData(taskData));
  }, [setData]);

  const taskData = data[taskId];

  return taskData == null ? null : (
    <div className={classes.container} container>
        <h2 className={classes.name}>{taskData.name} </h2>
        <div className={classes.info}>
              <div className={classes.infoline}>
                <div className={classes.infoname}>Size:</div><div className={classes.infoval}>{taskData.size}</div>
              </div>
              <div className={classes.infoline}>
                <div className={classes.infoname}>Type:</div><div className={classes.infoval}>{taskData.type}</div>
              </div>
              <div className={classes.infoline}>
                <div className={classes.infoname}>Number of images:</div>
                <div className={classes.infoval}>{taskData.number}</div> 
              
                {(taskData.type === "Identification") &&
                  <div>
                    <Button
                      variant="contained"
                      color="secondary"
                      className={classes.button}
                      startIcon={<Visibility />}
                      component={Link}
                      to={"/tasks/"+ taskId + "/identification"}
                    >
                      View
                    </Button>
                    <Button
                      variant="contained"
                      color="primary"
                      className={classes.button}
                      startIcon={<CloudDownloadIcon />}
                    >
                      Download
                    </Button>
                  </div>
                }
              </div>
              {(taskData.type === "Segmentation") &&
                <div>
                  <div className={classes.infoline}>
                    <div className={`${classes.infoname} ${classes.seal}`}>Seal (HQ):</div>
                    <div className={classes.infoval}>{taskData.numberHQ}</div> 
                    <Button
                      variant="contained"
                      color="secondary"
                      className={classes.button}
                      startIcon={<Visibility />}
                      component={Link}
                      to={"/tasks/"+ taskId + "/hq"}
                    >
                      View
                    </Button>
                    <Button
                      variant="contained"
                      color="primary"
                      className={classes.button}
                      startIcon={<CloudDownloadIcon />}
                    >
                      Download
                    </Button>
                    <Button
                      variant="contained"
                      color="primary"
                      className={classes.button + " " + classes.identifyButton}
                      startIcon={<NorppaIcon />}
                    >
                      Identify
                    </Button>
                  </div>
                  <div className={classes.infoline}>
                    <div className={`${classes.infoname} ${classes.seal}`}>Seal (LQ):</div>
                    <div className={classes.infoval}>{taskData.numberLQ}</div> 
                    <Button
                      variant="contained"
                      color="secondary"
                      className={classes.button}
                      startIcon={<Visibility />}
                      component={Link}
                      to={"/tasks/"+ taskId + "/lq"}
                    >
                      View
                    </Button>
                    <Button
                      variant="contained"
                      color="primary"
                      className={classes.button}
                      startIcon={<CloudDownloadIcon />}
                    >
                      Download
                    </Button>
                  </div>
                  <div className={classes.infoline}>
                    <div className={`${classes.infoname} ${classes.seal}`}>No seal:</div>
                    <div className={classes.infoval}>{taskData.numberNo}</div> 
                    <Button
                      variant="contained"
                      color="secondary"
                      className={classes.button}
                      startIcon={<Visibility />}
                      component={Link}
                      to={"/tasks/"+ taskId + "/no"}
                    >
                      View
                    </Button>
                    <Button
                      variant="contained"
                      color="primary"
                      className={classes.button}
                      startIcon={<CloudDownloadIcon />}
                    >
                      Download
                    </Button>
                  </div>
                </div>
              } 
      </div>
    </div>
  );
}