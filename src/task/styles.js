import { makeStyles } from '@material-ui/core/styles';
import { green } from '@material-ui/core/colors';

const useStyles = makeStyles(theme => ({
    name: {
      color: theme.palette.common.black,
      width: '100%',
      fontSize: '32px',
      marginBottom: 50
    },
    container: {
      maxWidth: 1200,
      position: 'relative',
      margin: '40px auto'
    },
    info: {
      maxWidth: 960,
      position: 'relative',
      margin: '0 auto',
      fontSize: '24px'
    },
    infoline: {
      display: 'flex',
      marginTop: 20
    },
    infoname: {
      width: '250px'
    },
    infoval: {
      textAlign: 'right',
      width: '150px'
    },
    seal: {
      paddingLeft: '40px',
      boxSizing: "border-box",
      fontSize: 20
    },
    button: {
      marginLeft: 40,
    },
    identifyButton: {
      background: green[500],
      borderColor: green[500],

      '&:hover': {
        backgroundColor: green[700],
        borderColor: green[700],
      },
    }
    
  }));

export default useStyles;