import React from 'react';
import SvgIcon from '@material-ui/core/SvgIcon';

export function NorppaIcon(props) {
  return (
    <SvgIcon {...props}>
      <path d="M18 13v7H4V6h5.02c.05-.71.22-1.38.48-2H4c-1.1 0-2 .9-2 2v14c0 1.1.9 2 2 2h14c1.1 0 2-.9 2-2v-5zm-1 6H5C5 8 17 8 17 19zm2.3-10.11c.44-.7.7-1.51.7-2.39C20 4.01 17.99 2 15.5 2S11 4.01 11 6.5s2.01 4.5 4.49 4.5c.88 0 1.7-.26 2.39-.7L21 13.42 22.42 12zM15.5 9a2.5 2.5 0 0 1 0-5 2.5 2.5 0 0 1 0 5z"></path>
      <circle cx="9.257" cy="14.857" r=".493" fill="#000"></circle>
      <circle cx="12.743" cy="14.869" r=".493" fill="#000"></circle>
      <path d="M11 16.374l-.176-.305-.177-.306h.706l-.177.306z" fill="#000"></path>
      <g fill="none" stroke="#000" stroke-width=".1">
          <path d="M9.717 17.595c.53-.127 1.122-.136 1.283-1.628M12.283 17.595c-.53-.127-1.122-.136-1.283-1.628" stroke-width=".17432000000000003"></path>
      </g>
      <g fill="none" stroke="#000" stroke-width=".1"><path d="M13.237 16.828L16 15.792M13.279 17.028l2.95-.005M13.237 17.228L16 18.264" stroke-width=".17432000000000003"></path>
        <g>
          <path d="M8.763 16.828L6 15.792M8.721 17.028l-2.95-.005M8.763 17.228L6 18.264" stroke-width=".17432000000000003"></path>
        </g>
      </g>
    </SvgIcon>
  );
}