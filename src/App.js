import React from 'react';
import './App.css';
import NavPanel from './navTabs';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import UploadForm from './uploadDataset';
import TasksTable from './tasksTable';
import Task from './task';
import Dataset from './dataset';
import Database from './database';
import IdentificationView from './identificationView';
import IdentificationChange from './identificationChange';
import {
  Switch,
  Route,
  Redirect,
  BrowserRouter as Router
} from "react-router-dom";


const useStyles = makeStyles(theme => ({

  menuButton: {
    marginRight: theme.spacing(2),
  }
}));


export default function NorppaAppBar() {
  const classes = useStyles();

  return (
    <Router basename={process.env.PUBLIC_URL}>
      <div className={classes.root}>
        <AppBar position="static">
          <Toolbar>
            <IconButton edge="start" className={classes.menuButton} color="inherit" aria-label="menu">
              <MenuIcon />
            </IconButton>
            <Typography variant="h6" className={classes.title}>
              Web Coexist
            </Typography>
          </Toolbar>
          <NavPanel />
        </AppBar>
        <Switch>
          <Route exact path="/"><Redirect to="/upload" /></Route>
          <Route path="/upload" component={UploadForm} />
          <Route path="/database" component={Database} />
          <Route path="/tasks/:taskId/identification/:imageId/change" component={IdentificationChange} />
          <Route path="/tasks/:taskId/identification" component={IdentificationView} />
          <Route path="/tasks/:taskId/:datasetType" component={Dataset} />
          <Route path="/tasks/:taskId" component={Task} />
          <Route path="/tasks" component={TasksTable} />
        </Switch>
      </div>
    </Router>
  );
}
