import React, {Component} from 'react'
import {DropzoneArea} from 'material-ui-dropzone'

 
class FileUploader extends Component{
  constructor(props){
    super(props);
    this.state = {
      files: []
    };
  }
  handleChange(files){
    this.setState({
      files: files
    });
  }
  render(){
    return (
      <DropzoneArea 
        dropzoneClass="dropzone"
        dropzoneText="Drag and drop your dataset here or click"
        onChange={this.handleChange.bind(this)}
        />
    )  
  }
} 

export default FileUploader;