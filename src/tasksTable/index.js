import React from 'react';
import useStyles from './styles.js';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import StyledTableCell from './styledTableCell';
import StyledTableRow from './styledTableRow';
import {
    Link
  } from "react-router-dom";


function createData(name, date, type, size, status) {
    return { name, date, type, size, status };
  }
  
const rows = [
    createData('Harmaasaari_080619', '10.12.2020', 'Cleaning', '3GB', 'Progress'),
    createData('Suurvehkosaari_250519', '03.09.2019', 'Identification', '1GB', 'Finished'),
];


export default function TasksTable() {
    const classes = useStyles();

    return (
        <TableContainer className={classes.container} component={Paper}>
        <Table className={classes.table} aria-label="tasks table">
            <TableHead>
                <TableRow>
                    <StyledTableCell>Date</StyledTableCell>
                    <StyledTableCell>Name</StyledTableCell>
                    <StyledTableCell>Type</StyledTableCell>
                    <StyledTableCell>Size</StyledTableCell>
                    <StyledTableCell>Status</StyledTableCell>
                </TableRow>
            </TableHead>
            <TableBody>
            {rows.map((row,index) => (
                <StyledTableRow key={row.name}>
                    <StyledTableCell component="th" scope="row">
                        {row.date}
                    </StyledTableCell>
                    <StyledTableCell><Link className={classes.name} to={"/tasks/"+ index} >{row.name}</Link></StyledTableCell>
                    <StyledTableCell>{row.type}</StyledTableCell>
                    <StyledTableCell>{row.size}</StyledTableCell>
                    <StyledTableCell><span className={row.status === 'Progress'? classes.progress : classes.finished}>{row.status}</span></StyledTableCell>
                </StyledTableRow>
            ))}
            </TableBody>
        </Table>
        </TableContainer>
    );
}