import {makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme =>({
    table: {
      minWidth: 700,
    },
    container: {
        maxWidth: 1200,
        position: 'relative',
        margin: '40px auto'
    },
    name: {
        color: theme.palette.common.black,
        '&:hover': {
            color: theme.palette.secondary.dark,
        }
    },
    progress: {
        background: theme.palette.warning.main,
        color: theme.palette.common.white,
        padding: 8,
        borderRadius: 10,
        boxSizing: 'border-box'
    },
    finished: {
      background: theme.palette.success.main,
      color: theme.palette.common.white,
      padding: 8,
      borderRadius: 10,
      boxSizing: 'border-box'
  }
}));

export default useStyles;