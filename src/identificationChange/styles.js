import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
    container: {
      maxWidth: 1200,
      position: 'relative',
      margin: '40px auto'
    },
    imageItem: {
      position: "relative",
      width: "calc(100%/3 - 20px)",
      marginRight: "20px",
      marginBottom: "20px"
    },
    imagesContainer: {
      display: "flex",
      flexWrap: "wrap"
    },
    queryImage: {
      width: "100%",
    },
    matchImage: {
      width: "100%",
    },
    image: {
      width: "100%",
      top: 0,
      boxShadow: "0px",
      transition: "all 0.25s ease",

      "&:hover" : {
        cursor: "pointer",
        boxShadow: "0px 0px 6px 5px rgba(0, 0, 0, .2)"
      }
    },
    queryImageContainer: {
      width: "400px",
      marginRight: 40
    }, 
    queryContainer: {
      display: "flex",
    }, 
    button: {
      marginRight: "10px",
      marginTop: "5px"
    },
    changeButton: {
      marginTop: 10
    },
    sealId: {
      marginTop: 18
    },
    menu: {
      display: "flex",
      justifyContent: "space-between"
    }
    
  }));

export default useStyles;