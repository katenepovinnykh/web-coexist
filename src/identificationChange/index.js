import React from 'react';
import useStyles from './styles';
import { useState, useEffect } from 'react';
import {getIdentificationData} from '../server';
import { Button } from '@material-ui/core';


export default function Database({match}) {
  const classes = useStyles();
  const [data, setData] = useState(null);
  const imageId = match.params.imageId;
  const [currectSeal, setSeal] = useState('');

  const handleClick = data => {
    setSeal(data);
  };

  useEffect(() => {
    getIdentificationData(imageId).then(taskData => 
      {
        setData(taskData);
        setSeal(taskData.match)
      });
  }, [setData, imageId]);

  return data == null ? null : ( 
    <div className={classes.container} container>
        <h2>Change id</h2>
        <div className={classes.queryContainer}>
          <div className={classes.queryImageContainer}>
            <h3>Query image</h3>
            <img src={process.env.PUBLIC_URL + data.seal} className={classes.queryImage} alt="/" />
          </div>
          <div className={classes.queryImageContainer}>
            <h3>Match image: {currectSeal.id}</h3>
            <img src={process.env.PUBLIC_URL + currectSeal.src} className={classes.matchImage} alt="/" />
          </div>
        </div>
        <Button variant="outlined" color="primary" className={classes.changeButton}>
          Correct
        </Button>
        <h2>Closest matches:</h2>
        <div className={classes.imagesContainer} >
          {data.results.map((seal,i) => (
            seal.images.map((image,j) => (
                (data.idList.includes(image.id)) && (
                  <div key={j} className={classes.imageItem}>
                    <img src={process.env.PUBLIC_URL + image.src} onClick={() => handleClick({src: image.src, id: seal.id})} className={classes.image} alt=""/>
                    <div className={classes.menu}>
                      <div className={classes.sealId}>{seal.id}</div>
                      <Button variant="outlined" color="primary" className={classes.changeButton}>
                        Correct
                      </Button>
                    </div>
                  </div>
                )
              ))
            ))}
        </div>
        </div>
    
  );
}