import React from 'react';
import { Link } from 'react-router-dom';
import useStyles from './styles';
import { useState, useEffect } from 'react';
import {getDatabaseData} from '../server';
import { Button } from '@material-ui/core';


export default function Database({match}) {
  const classes = useStyles();
  const taskId = match.params.taskId;
  const [data, setData] = useState([]);



  useEffect(() => {
    async function getData() {
      const data = await getDatabaseData();
      setData(data);
  }
  getData();
  }, []);

  return (
    <div className={classes.container} container>
        <h2>Identification results</h2>
        {data.map((seal,i) => (
          <div key={i}>
            <h3>{seal.id} </h3>
            <div className={classes.imagesContainer} >
              {seal.images.map((image,j) => (
                <div key={j} className={classes.imageItem}>
                  <img src={process.env.PUBLIC_URL + image.src} className={classes.image} alt=""/>
                  <Button variant="outlined" color="primary" className={classes.addButton} component={Link}
                      to={"/tasks/" + taskId + "/identification/" + image.id + "/change"}>
                    Change id
                  </Button>
                </div>
              ))}
            </div>
          </div>
        ))}
    </div>
  );
}