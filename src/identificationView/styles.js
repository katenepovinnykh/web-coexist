import { makeStyles } from '@material-ui/core/styles';
import { green} from '@material-ui/core/colors';

const useStyles = makeStyles(theme => ({
    container: {
      maxWidth: 1200,
      position: 'relative',
      margin: '40px auto'
    },
    imageItem: {
      position: "relative",
      width: "calc(100%/3 - 20px)",
      marginRight: "20px",
      marginBottom: "20px"
    },
    imagesContainer: {
      display: "flex",
      flexWrap: "wrap"
    },
    image: {
      width: "100%",
      top: 0,
    },
    button: {
      marginRight: "10px",
      marginTop: "5px"
    },
    addButton: {
      borderColor: green[500],
      color: green[700],
      '&:hover': {
        backgroundColor: green[50],
        color: green[700],
        borderColor: green[700],
      },
    }
    
  }));

export default useStyles;