import React from 'react';
import useStyles from './styles';
import { useState, useEffect } from 'react';
import {getDatasetData} from '../server';
import { Button } from '@material-ui/core';


export default function Dataset({match}) {
  const classes = useStyles();
  const [data, setData] = useState([]);
  // const taskData = tasksMap[match.params.taskId];
  const names={'hq': "High quality", 'lq': "Low quality", 'no': "No seals"}
  const datasetType = match.params.datasetType;
  


  useEffect(() => {
    async function getData() {
      const data = await getDatasetData(datasetType);
      setData(data);
  }
  getData();
  }, [datasetType]);

  return (
    <div className={classes.container} container>
        <h2>{names[datasetType]} images</h2>
        <div className={classes.imagesContainer} >
            {data.map((image,index) => (
              <div key={index} className={classes.imageItem}>
                <img src={process.env.PUBLIC_URL + image.src} className={classes.image} alt=""/>
                {(datasetType !== 'hq') &&
                  <Button variant="outlined" size="small" color="primary" className={classes.button + " " + classes.greenButton}>
                    High quality
                  </Button>
                }{         
                  (datasetType !== 'lq') &&
                  <Button variant="outlined" size="small" color="primary" className={classes.button + " " + classes.yellowButton}>
                    Low Quality
                  </Button>
                } {
                  (datasetType !== 'no') &&
                  <Button variant="outlined" size="small" color="secondary" className={classes.button}>
                    No seal
                  </Button>
                }
              </div>
            ))}
        </div>
    </div>
  );
}