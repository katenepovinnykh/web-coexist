import React from 'react';
import useStyles from './styles';
import { useState, useEffect } from 'react';
import {getDatabaseData} from '../server';
import { Button } from '@material-ui/core';
import AddIcon from '@material-ui/icons/Add';


export default function Database({match}) {
  const classes = useStyles();
  const [data, setData] = useState([]);
  // const taskData = tasksMap[match.params.taskId];
  


  useEffect(() => {
    async function getData() {
      const data = await getDatabaseData();
      setData(data);
  }
  getData();
  }, []);

  return (
    <div className={classes.container} container>
        <div className={classes.header}>
          <h1 className={classes.title}>Database</h1>
          <Button variant="outlined" color="primary" className={classes.addButton} startIcon={<AddIcon />}>
            Add a new seal
          </Button>
        </div>
        {data.map((seal,i) => (
          <div key={i}>
            <h3 className={classes.sealId}>{seal.id} </h3>
            <div className={classes.imagesContainer} >
              {seal.images.map((image,j) => (
                <div key={j} className={classes.imageItem}>
                  <img src={process.env.PUBLIC_URL + image.src} className={classes.image} alt=""/>
                </div>
              ))}
            </div>
            <Button variant="outlined" color="primary" className={classes.addButton} startIcon={<AddIcon />}>
              Upload more images
            </Button>
          </div>
        ))}
    </div>
  );
}