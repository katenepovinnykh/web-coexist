import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
    root: {
      flexGrow: 1,
      backgroundColor: theme.palette.background.paper,
  
      "& > .tabs": {
          padding: '0 24px',
          backgroundColor: theme.palette.primary.main
      }
    }
  }));

  export default useStyles;
