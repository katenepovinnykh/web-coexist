import React from 'react';
import Tabs from '@material-ui/core/Tabs';
import useStyles from './styles.js';
import Tab from '@material-ui/core/Tab';
import {
    Link,
    useLocation
  } from "react-router-dom";

function LinkTab(props) {
    return (
      <Tab
        component={Link}
        {...props}
      />
    );
  }

export default function () {
    const classes = useStyles();
    const location = useLocation();
  
    return (
      <div className={classes.root}>
          <Tabs
              value={location.pathname}
              aria-label="nav tabs"
              className="tabs"
          >
              <LinkTab label="Upload" value="/upload" to="/upload" />
              <LinkTab label="Tasks" value="/tasks" to="/tasks" />
              <LinkTab label="Database" value="/database" to="/database" />
          </Tabs>
          
      </div>
    );
  }