import { makeStyles } from '@material-ui/core/styles';

export default makeStyles(theme => ({
    root: {
      marginTop: 40,
      '& > *': {
        margin: theme.spacing(1),
      },
      '& > .dropzone': {
          'margin-top': 30,
          'margin-bottom': 20,
          height: 150,
          'min-height': 150,
          'max-height': 150,
          background: 'white',
          'border-radius': 10,
          color: '#aaa',
          'position': 'relative'
      },
      '& > .dropzone svg': {
          fill: theme.palette.primary.main
      },
      formControl: {
        marginTop: 20,
      }
    },
}));