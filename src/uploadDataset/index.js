import React from 'react';
import useStyles from './styles';
import { FormControl, Grid } from '@material-ui/core';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import FormHelperText from '@material-ui/core/FormHelperText';
import FileUploader from '../fileUploader';
import Button from '@material-ui/core/Button';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';


export default function UploadDataset() {
  const classes = useStyles();
  const [type, setType] = React.useState('');

  const handleChange = event => {
    setType(event.target.value);
  };

  return (
    <Grid container justify="center">
        <form className={classes.root} noValidate autoComplete="off">
            <FormControl fullWidth={true} className={classes.formControl}>
                <InputLabel htmlFor="name">Name</InputLabel>
                <Input id="name" />
                <FormHelperText id="name-helper-text">Your full name</FormHelperText>
            </FormControl>
            <FormControl fullWidth={true} className={classes.formControl}>
                <InputLabel htmlFor="project_name">Project Name</InputLabel>
                <Input id="project_name" />
                <FormHelperText id="name-helper-text">The name of dataset</FormHelperText>
            </FormControl>
            <FormControl fullWidth={true} className={classes.formControl}>
                <InputLabel id="select-type">Task type</InputLabel>
                <Select
                    labelId="type"
                    id="type"
                    value={type}
                    onChange={handleChange}
                >
                    <MenuItem value={"Cleaning"}>Cleaning</MenuItem>
                    <MenuItem value={"Identification"}>Identification</MenuItem>
                </Select>
            </FormControl>
            <FormControl fullWidth={true} className={classes.formControl}>
                <InputLabel htmlFor="email">Email address</InputLabel>
                <Input id="email" />
                <FormHelperText id="email-helper-text">We will send notification here after processing is done</FormHelperText>
            </FormControl>
            <FileUploader />
            <Button variant="contained" color="primary" size="large">
                UPLOAD
            </Button>
        </form>
    </Grid>
  );
}