
const downloadData = async () => {
  const response = await fetch(process.env.PUBLIC_URL + '/data.json');
  const data = await response.json();
  document.server_data = data;
}

export async function getDatasetData (datasetType) {
    if (document.server_data == null) {
      await downloadData();
    }
    if ('images' in document.server_data) {
      return document.server_data.images.filter((image) => image.category === datasetType);
    }
    return null
  }

  export async function getDatabaseData () {
    if (document.server_data == null) {
      await downloadData();
    }
    if ('database' in document.server_data) {
      const result = document.server_data.database.sort((a,b) => {
        if (a.id < b.id) {
          return -1;
        }
        if (a.id > b.id) {
          return 1;
        }
        return 0;
      });
      return result;
    }
    return null
  }

  export async function getTasksData () {
    if (document.server_data == null) {
      await downloadData();
    }
    if ('tasks' in document.server_data) {
      return document.server_data.tasks;
    }
    return null
  }

  export async function getIdentificationData (imageId) {
    console.log(imageId)
    if (document.server_data == null) {
      await downloadData();
    }
    const result = {}
    if ('database' in document.server_data) {
      for (let seal of document.server_data.database) {
        let foundImage = false;
        let foundMatch = false;
        for (let image of seal.images) {
          if (!foundImage && image.id === parseInt(imageId)) {
            result.seal = image.src;
            foundImage = true;
          }
          if (!foundMatch && image.id === document.server_data.changeId[0]) {
            result.match ={src: image.src, id: seal.id};
            foundMatch = true;
          }
        }
        if (foundImage && foundMatch) break;
        }
    }
    console.log(document.server_data);
    if ('changeId' in document.server_data) {
      const idList = document.server_data.changeId;
      result['results'] = document.server_data.database.filter((seal) => seal.images.some(image=> idList.includes(image.id)));
      result['idList'] = idList;
      return result
    }
    return null
  }